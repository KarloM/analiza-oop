﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp6
{
   
    public partial class Form1 : Form
    {
        bool player = true;
        bool thereisawinner = false;
        int brojac = 0;
        public Form1()
        {
            InitializeComponent();
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "")
            {
                if (player == true) { button1.Text = "X"; label1.Text = "O potez"; }
                else { button1.Text = "O"; label1.Text = "X potez"; }
                player = !player;
                brojac++;
                checkForWinner();
            }
            else return;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (button2.Text == "")
            {
                if (player == true) button2.Text = "X";
                else button2.Text = "O";
                player = !player;
                brojac++;
                checkForWinner();
            }
            else return;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (button3.Text == "")
            {
                if (player == true) button3.Text = "X";
                else button3.Text = "O";
                player = !player;
                brojac++;
                checkForWinner();
            }
            else return;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (button4.Text == "")
            {
                if (player == true) button4.Text = "X";
                else button4.Text = "O";
                player = !player;
                brojac++;
                checkForWinner();
            }
           else return;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (button5.Text == "")
            {
                if (player == true) button5.Text = "X";
                else button5.Text = "O";
                player = !player;
                brojac++;
                checkForWinner();
            }
            else return;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (button6.Text == "")
            {
                if (player == true) button6.Text = "X";
                else button6.Text = "O";
                player = !player;
                brojac++;
                checkForWinner();
            }
            else return;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (button7.Text == "")
            {
                if (player == true) button7.Text = "X";
                else button7.Text = "O";
                player = !player;
                brojac++;
                checkForWinner();
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (button8.Text == "")
            {
                if (player == true) button8.Text = "X";
                else button8.Text = "O";
                player = !player;
                brojac++;
                checkForWinner();
            }
            else return;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (button9.Text == "")
            {
                if (player == true) button9.Text = "X";
                else button9.Text = "O";
                player = !player;
                brojac++;
                checkForWinner();
            }
            else return;
        }
        private void checkForWinner()
        {
            
           
            if ((button1.Text !="" && button1.Text == button2.Text) && (button2.Text == button3.Text)) thereisawinner = true;
            else if ((button1.Text != "" && button1.Text == button4.Text) && (button4.Text == button7.Text)) thereisawinner = true;
            else if ((button2.Text != "" && button2.Text == button5.Text) && (button5.Text == button8.Text)) thereisawinner = true;
            else if ((button3.Text != "" && button3.Text == button6.Text) && (button6.Text == button9.Text)) thereisawinner = true;
            else if ((button1.Text != "" && button1.Text == button5.Text) && (button5.Text == button9.Text)) thereisawinner = true;
            else if ((button7.Text != "" && button7.Text == button5.Text) && (button5.Text == button3.Text)) thereisawinner = true;
            else if ((button4.Text != "" && button4.Text == button5.Text) && (button5.Text == button6.Text)) thereisawinner = true;
            else if ((button7.Text != "" && button7.Text == button8.Text) && (button8.Text == button9.Text)) thereisawinner = true;

           


                if (thereisawinner)
                {
                    string winner;
                    if (!player) winner = "X je pobjedio";
                    else winner = "O je pobjedio";
                    MessageBox.Show(winner);
                
               
           }
            if (brojac == 9 && !thereisawinner) MessageBox.Show("Izjednaceno");
        }

    }
   
}
